var codigoSecreto = generacodigo();
console.log(codigoSecreto);
var codigoUser;
var counter = 0;
var intents = 1;


function generacodigo() {
  const num = [];
  let i = 0;
  while (i < 5) {
      let ran = Math.floor(Math.random() * 9) + 1;
      while (num.includes(ran)) {
          ran = Math.floor(Math.random() * 9) + 1;
      }
      num.push(ran);
  
      i++;
  }
  return num

}

function cuenta(codigo, num) {
  let veces = 0;
  for (let i = 0; i < codigo.length; i++) {
    if (codigo[i] == num) {
      veces++;
    }
  }
  return veces;
}

function estaNum(codigo, num, pos) {
  if (codigo[pos] == num) {
    return true;
  }
  return false;
}

function escodigoCorreto(codigoSecre, codigoUser) {
  let result = []
  for (let i = 0; i < codigoSecre.length; i++) {
    result.push(cuenta(codigoSecre, codigoUser[i]));
    if (result[i] > 0) {
      if (estaNum(codigoSecre, codigoUser[i], i)) {
        result[i] = "ok";
      }
    }
  }
  return result;
}

function acertado(resultado) {
  for (let i = 0; i < resultado.length; i++) {
    if (resultado[i] != 'ok') {
      return false;
    }
  }
  return true;
}

function comprovar_num(n) {
  return !(n.length != 5);
}
//comprova si el numero del ususari es correcte
function vali() {
  ins = document.getElementById('numero');

  const valor = ins.value;
  codigoUser = valor;

  while (!comprovar_num(codigoUser) || isNaN(codigoUser)) {
    alert("incorrecte ha de ser un numero i de 5 xifres")
    return 0;
  }
  codigoUser = codigoUser.split("")

  //verificamos si el codigo del user es correcto
  resultado = escodigoCorreto(codigoSecreto, codigoUser);
  return resultado
}

//crea el boto per reinicar la pagina al acabar el joc
function addReset(element) {
  var button = document.createElement("button");
  button.innerHTML = "Torna a Jugar";
  button.setAttribute("onclick", "location.reload()");
  element.appendChild(button);
}

//executa l'escript per fer focs artificials
function dynamicallyLoadScript(url) {
  var script = document.createElement("script");
  script.src = url;
  document.head.appendChild(script);
}

//bloqueja la pantalla per poder mostrar be els focs(requeriment del creador de l'escript(inportat))
function blockScreen() {
  var screen = document.getElementsByTagName('html')[0];
  screen.style.setProperty("overflow-y", "hidden");
  screen.style.setProperty("overflow-x", "hidden");
}

//afageix un cop perds o guanyes el numero final a les caselles superiors
function addFinalNum() {
  let p = document.querySelectorAll("#final .w20 div")
  for (var a = 0; a < 5; a++) {
    p[a].innerHTML = codigoSecreto[a];
  }
}

function addValdiate(pos){
  let p = document.querySelectorAll("#final .w20 div")
  p[pos].innerHTML = codigoSecreto[pos];
}


var fi = false;

function com() {

  if (!fi) {
    let inf = document.getElementById('info')

    inf.innerHTML = 'Intent ' + intents + '/5';
    let user = vali();

    if (user == 0) {
      return 0;
    }

    for (var i = 0; i < 5; i++) {
      var container = document.getElementById("" + counter + i);
      container.innerHTML += codigoUser[i];

      if (user[i] == "ok") {
        container.style.backgroundColor = '#04ca04 ';
        addValdiate(i)
      }
      if (user[i] >= 1) {
        container.style.backgroundColor = '#fbd333 ';
      }
    }
    const isOk = (currentValue) => currentValue == 'ok';

    if (user.every(isOk)) {
      inf.innerHTML = 'Has encertat el numero Felicitats!<br/>';
      addFinalNum();
      blockScreen();
      dynamicallyLoadScript('generator.js')
      inf.innerHTML += 'Has acabat el joc clica per tornar a jugar!';
      addReset(inf);
      fi = true;
    }
    if (intents > 4) {
      inf.innerHTML = 'Has fallat el numero!<br/>';
      inf.innerHTML += 'Has acabat el joc clica per tornar a jugar!';
      addReset(inf);
      addFinalNum();
      fi = true;
      return 0;
    }
    intents++;

    counter++;
  } else {
    return 0;
  }
}



